That repository proofs that [my OpenPGP key](https://keyoxide.org/A1472FC18CB7A6B5971F6D260E6AD690E6659C65) connects to this account.

```
opengpg4fpr:A1472FC18CB7A6B5971F6D260E6AD690E6659C65
```
